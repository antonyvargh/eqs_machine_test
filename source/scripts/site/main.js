$(window).on('load', function () {
    $('body').addClass('is-loaded');


    /* get json data */
    let contact, 
    company,
    email,
    fax,
    phoneNumber,
    address,
    isCurrentCompany,
    companyName,
    yearOfExperience,
    totalExperience,
    companyLocation;

    function getData(){
        $.ajax({
            url: 'empdetails.json',
            dataType: 'json',
            success: function(data) {
                $('#json__infocontent').html('');
                data.map(item=>{
                    contact = item.ContactDetails;
                    company = item.Company;
                    
                    contact.map(contactItem=>{
                        email = contactItem.Email;
                        phoneNumber = contactItem.PhoneNumber;
                        fax = contactItem.Fax;
                        address = contactItem.Address;
                    });
                    
                    $.each(
                        
                    )
                   
                    var rowMaker = '';
                    totalExperience = 0;
                    $.each(company, function(i,companydata){
                        isCurrentCompany = companydata.IsCurrentCompany;
                        companyName = companydata.CompanyName;
                        yearOfExperience = companydata.YearOfExperience;
                        companyLocation = companydata.Location;
                        totalExperience = totalExperience+yearOfExperience
                        rowMaker = rowMaker + 
                            '<table class="rowmaker">'
                               
                                +'<tr>'
                                    +'<td valign="top"><strong>Company Name: </strong>'+ companyName + '</td>'
                                +'</tr>'
                                +'<tr>'
                                    +'<td valign="top"><strong>Is Current: </strong>' + isCurrentCompany + '</td>'
                                +'</tr>'
                                +'<tr>'
                                    +'<td valign="top"><strong>Year of Experience: </strong>'+ yearOfExperience +'</td>'
                                +'</tr>'
                                +'<tr>'
                                    +'<td valign="top"><strong>Location: </strong>'+ companyLocation +'</td>'
                                +'</tr>'
                                
                            +'</table>'
                    });

                    // for single item
                    // company.map(companyItem=>{
                    //     isCurrentCompanyX = companyItem.IsCurrentCompany;
                    //     companyNameX = companyItem.CompanyName;
                    //     yearOfExperienceX = companyItem.YearOfExperience;
                    //     companyLocationX = companyItem.Location;

                       
                    // });

                    
                    var row = $(
                        '<tr>'
                            +'<td valign="top">' + item.ID +'</td>'
                            +'<td valign="top">' + item.Name +'</td>'
                            +'<td valign="top">' + item.LastName +'</td>'
                            +'<td valign="top">' + item.Age +'</td>'
                            +'<td valign="top">'
                                + '<table>'
                                + '<tr>'
                                        +'<td valign="top"><strong>Email: </strong> <br/>' + email +'</td>'
                                    +'</tr>'
                                    + '<tr>'
                                        +'<td valign="top"><strong>Phone Number: </strong> <br/>' + phoneNumber +'</td>'
                                    +'</tr>'
                                    + '<tr>'
                                        +'<td valign="top"><strong>Fax: </strong> <br/>' + fax + '</td>'
                                    +'</tr>'
                                    + '<tr>'
                                        +'<td valign="top"><strong>Address: </strong> <br/>' + address + '</td>'
                                    +'</tr>'
                                + '</table>'
                            +'</td>'
                            +'<td valign="top">'
                                + rowMaker +
                               
                                '<strong>Total Year of Experience : </strong>' + totalExperience 
                               
                            +'</td>'

                        + '</tr>'
                    )
                    
                    $('#json__infocontent').append(row);
                });

            
                /* alternate looping method */

                
                // for (var i=0; i<data.length; i++) {
                //     var contact = data[i].ContactDetails;
                //     var company =data[i].Company;
                //     for (var j=0; j<contact.length; j++){
                //         for(var k=0; k<company.length;k++){
                //             var row = $(
                //                 '<tr><td>' + data[i].ID+ '</td><td>' + data[i].Name + '</td><td>' + data[i].LastName + '</td><td>' + data[i].Age + '</td><td>' + contact[j].Email + ' <br/> ' + contact[j].PhoneNumber +' <br/> ' +  contact[j].Fax + ' <br/> ' +  contact[j].Address +  '</td><td>'  + company[k].IsCurrentCompany +' <br/> ' + company[k].CompanyName +' <br/> ' + company[k].YearOfExperience +' <br/> ' + company[k].Location +' <br/> ' + company[k].YearOfExperience + '</td></tr>');
                //         }
                //     }
                //     $('#json__infocontent').append(row);
                // }
                

            },
            error: function(jqXHR, textStatus, errorThrown){
                alert('Error: ' + textStatus + ' - ' + errorThrown);
            }
        });
    }

    $('#show_info').click(function(e){
        e.preventDefault();
        $('.json__tabewrap').slideToggle();
        $('#show, #hide').toggle();
        getData();
    });
}); 

$(document).ready(function(){
    
    $('#year').html((new Date).getFullYear());

    // slider
    $('.hero--wrap').slick({
        prevArrow: $('.btn--prev'),
        nextArrow: $('.btn--next')
    });


    /*nav bar */
    $('.btn--nav').click(function(e){
        e.preventDefault();
        $('.main__menu').toggleClass('menu_open');
        $(this).toggleClass('menu_opened')
    });

    $('.main__menu ul li').click(function(){
        console.log('fire');
        var menuItem = $(this);
        menuItem.find('.submenu').slideToggle();
    })
    $(window).scroll(function(){
        var scroll = $(this).scrollTop();
        if (scroll > 200){
            $('.header, .header__wrap').addClass('floating');
        }else{
            $('.header, .header__wrap').removeClass('floating');
        }
    });

    $(".back-top").click(function () {
        $("html, body").animate({scrollTop: 0}, 1000);
     });
})
